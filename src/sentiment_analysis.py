import itertools

import tweepy
import re
from datetime import datetime
from textblob import TextBlob

from tweepy import OAuthHandler


class TwitterClient(object):

    def __init__(self):

        # Class constructor or initialization method.
        # keys and tokens from the Twitter Dev Console
        # keys and tokens from the Twitter Dev Console
        consumer_key = 'xxxxxxxxxxxxx'
        consumer_secret = 'xxxxxxxxxxxxx'
        access_token = 'xxxxxxxxxxxxx-xxxxxxxxxxxxx'
        access_token_secret = 'xxxxxxxxxxxxx'

        # attempt authentication
        try:
            # create OAuthHandler object
            self.auth = OAuthHandler(consumer_key, consumer_secret)
            # set access token and secret
            self.auth.set_access_token(access_token, access_token_secret)
            # create tweepy API object to fetch tweets
            self.api = tweepy.API(self.auth)
        except:
            print("Error: Authentication Failed")

    def get_tweets(self, query, count=10):
        # Main function to fetch tweets and parse them.

        # empty list to store parsed tweets
        tweets = []

        try:
            # call twitter api to fetch tweets
            fetched_tweets = self.api.search(q=query, count=count)

            # parsing tweets one by one
            for tweet in fetched_tweets:
                # empty dictionary to store required params of a tweet
                parsed_tweet = {}

                # saving text of tweet
                parsed_tweet['text'] = tweet.text
                parsed_tweet['date'] = tweet.created_at

                # appending parsed tweet to tweets list
                if tweet.retweet_count > 0:
                    # if tweet has retweets, ensure that it is appended only once
                    if parsed_tweet['text'] not in tweets:
                        tweets.append(parsed_tweet)
                else:
                    tweets.append(parsed_tweet)

            # return parsed tweets
            return tweets

        except tweepy.TweepError as e:
            # print error (if any)
            print("Error : " + str(e))


class AnalyseSentiment(object):
    def __init__(self):
        pass

    def clean_tweet(self, tweet):
        '''
        Utility function to clean tweet text by removing links, special characters
        using simple regex statements.
        '''
        return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])| (\w+:\ / \ / \S+)", " ", tweet).split())

    def evaluateTweetSentiment(self, tweets):

        sentiment_score = {"positive": 0, "negative": 0, "neutral": 0}

        for tweet in tweets:
            analysis = TextBlob(self.clean_tweet(tweet["text"]))
            sentivent_value = analysis.sentiment.polarity

            if sentivent_value > 0:
                sentiment_score["positive"] += 1
            elif sentivent_value == 0:
                sentiment_score["neutral"] += 1
            elif sentivent_value < 0:
                sentiment_score["negative"] += 1

        # set sentiment
        return sentiment_score

    def sort_by_date(self, tweets):

        f = lambda x: x["date"].date()
        sorted_tweet = itertools.groupby(sorted(tweets, key=f), f)

        return sorted_tweet

    def get_tweet_sentiment(self, tweets, date_from, date_to):

        sorted_tweets = self.sort_by_date(tweets)

        result = {}

        for date, sorted_tweet in sorted_tweets:
            if date_from.date() <= date <= date_to.date():
                key = date.strftime('%d/%m/%Y')
                result[key] = self.evaluateTweetSentiment(sorted_tweet)

        return result


if __name__ == "__main__":
    query = input("search any brand or name in twitter:\n")

    while query != "quit":
        api = TwitterClient()
        count = 10000
        date_from = datetime.strptime('Jan 1 2018  12:00AM', '%b %d %Y %I:%M%p')
        date_to = datetime.strptime('Feb 10 2018  12:00AM', '%b %d %Y %I:%M%p')

        tweets = api.get_tweets(query, count)

        sentiments = AnalyseSentiment()

        reviews = sentiments.get_tweet_sentiment(tweets, date_from, date_to)

        print("\n", query)
        for key, review in reviews.items():
            total = (review["positive"] + review["negative"] + review["neutral"])
            positiveReview = (review["positive"] / total) * 100
            negative_review = (review['negative'] / total) * 100

            print("\n", key)
            print("Positive =>", positiveReview, "%")
            print("negative =>", negative_review, "%")

        query = input("\nType quit or any input to search:\n")
